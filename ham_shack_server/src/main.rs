extern crate hyper;

use std::sync::Mutex;
use std::sync::mpsc::{channel, Sender};
use hyper::server::{Handler, Server, Request, Response};

struct SenderHandler {
    sender: Mutex<Sender<&'static str>>
}

impl Handler for SenderHandler {
    fn handle(&self, req: Request, res: Response) {
        self.sender.lock().unwrap().send("start").unwrap();
        res.send(b"Hello World!").unwrap();
    }
}

fn main() {
    println!("Ham Shack Server");

    let (tx, rx) = channel();
    Server::http("192.168.10.191:8080").unwrap().handle(SenderHandler {
        sender: Mutex::new(tx)
    }).unwrap();
}
