# Ham Shack
Open source ham radio shack software
[![build status](https://gitlab.com/w41dev/ham_shack/badges/master/build.svg)](https://gitlab.com/w41dev/ham_shack/commits/master)
[![coverage report](https://gitlab.com/w41dev/ham_shack/badges/master/coverage.svg)](https://gitlab.com/w41dev/ham_shack/commits/master)
